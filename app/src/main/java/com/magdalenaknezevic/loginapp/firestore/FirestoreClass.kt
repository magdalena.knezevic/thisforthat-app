package com.magdalenaknezevic.loginapp.firestore

/*import android.app.Activity
import android.content.ContentValues.TAG
import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import android.util.Log
import android.widget.EditText*/
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.magdalenaknezevic.loginapp.R
import com.magdalenaknezevic.loginapp.activites.*
import com.magdalenaknezevic.loginapp.models.Product
import com.magdalenaknezevic.loginapp.models.User
import com.magdalenaknezevic.loginapp.utils.Constants

class FirestoreClass() : AppCompatActivity() {

   /* private val db = Firebase.firestore
    //private val mFireStore = FirebaseFirestore.getInstance()


    fun registerUser(activity: SignUpActivity, userInfo: User){
        db.collection(Constants.USERS)
            .add(userInfo)
            .addOnSuccessListener { document ->
                val id = document.id
                activity.userRegistrationSuccess()
            }
            .addOnFailureListener{ e ->
                Log.e(
                    activity.javaClass.simpleName,
                    "Error while registering the user",
                    e
                )
            }
    }

    fun addProduct(activity: AddNewProductActivity, productInfo: Product, userInfo: String){
        db.collection(Constants.USERS)
            .document(userInfo)
            .collection(Constants.PRODUCTS)
            .document("Test too")
            .set(productInfo, SetOptions.merge())
            .addOnSuccessListener{
                activity.addProductSuccess()
            }
            .addOnFailureListener{ e ->
                    Log.e(
                        activity.javaClass.simpleName,
                        "Error while adding new product",
                        e
                    )
            }
    }

    fun getProductId(): String{
        val currentProduct = FirebaseFirestore.getInstance()

        var currentProductID = ""
        if(currentProduct != null){
            currentProductID = getProductId()
        }
        return currentProductID
    }

    fun getProductDetails(activity: Activity){
        db.collection(Constants.PRODUCTS)
            .document(getProductId())
            .get()
            .addOnSuccessListener { document ->
                Log.i(activity.javaClass.simpleName, document.toString())

                val product = document.toObject(Product::class.java)!!

                val sharedPreferences =
                    activity?.getSharedPreferences(
                        Constants.THISFORTHAT_PREFERENCES,
                        Context.MODE_PRIVATE
                    )

                val editor: SharedPreferences.Editor = sharedPreferences!!.edit()
                editor.putString(
                    Constants.PRODUCT_NAME,
                    "${product.productName}"
                )
                editor.putString(
                    Constants.PRODUCT_CATEGORY,
                    "${product.productCategory}"
                )
                editor.putString(
                    Constants.PRODUCT_DESCRIPTION,
                    "${product.productDescription}"
                )
                editor.apply()


                when(activity){
                    is AddNewProductActivity -> {
                        activity.productAddedSuccess(product)
                    }
                }
            }
            .addOnFailureListener{ e ->
                when(activity){
                    is AddNewProductActivity -> {
                        activity.hideProgressDialog()
                    }
                }
                Log.e(
                    activity.javaClass.simpleName,
                    "Error while adding new product!",
                    e
                )

            }
    }


    fun getCurrentUserID(): String{
        val currentUser = FirebaseAuth.getInstance().currentUser

        var currentUserID = ""
        if(currentUser != null){
            currentUserID = currentUser.uid
        }
        return currentUserID
    }

    fun getUserDetails(activity: Activity){
        db.collection(Constants.USERS)
            .document(getCurrentUserID())
            .get()
            .addOnSuccessListener { document ->
                Log.i(activity.javaClass.simpleName, document.toString())

                val user = document.toObject(User::class.java)!!

                val sharedPreferences =
                    activity.getSharedPreferences(
                        Constants.THISFORTHAT_PREFERENCES,
                        Context.MODE_PRIVATE
                    )

                val editor: SharedPreferences.Editor = sharedPreferences.edit()
                //Key: logged_in_username   Value: Dalia Dalic
                editor.putString(
                    Constants.LOGGED_IN_USERNAME,
                    "${user.firstName}  ${user.lastName}"
                )
                editor.putString(
                    Constants.LOGGED_IN_EMAIL,
                    "${user.email}"
                )
                editor.apply()


                when(activity){
                    is SignInActivity -> {
                        activity.userLoggedSuccess(user)
                    }
                    /*is MainActivity -> {

                    }
                     */
                }
            }
            .addOnFailureListener{ e ->
                when(activity){
                    is SignInActivity -> {
                        activity.hideProgressDialog()
                    }
                    /* ovdje dodati isti activity koji je u addOnSuccessListener i sakriti progressIDalog*/
                }
                Log.e(
                    activity.javaClass.simpleName,
                    "Error while getting user details!",
                    e
                )

            }
    }



    fun updateUserProfileData(activity: Activity, userHashMap: HashMap<String, Any>){
        db.collection(Constants.USERS)
            .document(getCurrentUserID())
            .update(userHashMap)
            .addOnSuccessListener {
                when(activity){
                    is UserProfileActivity -> {
                        activity.userProfileUpdateSuccess()
                    }
                }
            }
            .addOnFailureListener{ e ->
                when(activity){
                    is UserProfileActivity -> {
                        activity.hideProgressDialog()
                    }
                }
                Log.e(
                activity.javaClass.simpleName,
                "Error while updating the user details.",
                e)
            }
    }

    fun uploadImageToCloudStorage(activity: Activity, imageFileURI: Uri?){
        val sRef: StorageReference = FirebaseStorage.getInstance().reference.child(
            Constants.USER_PROFILE_IMAGE + System.currentTimeMillis() + "."
                     + Constants.getFileExtension(
                activity,
                imageFileURI
            )
        )
        sRef.putFile(imageFileURI!!).addOnSuccessListener { taskSnapshot ->
            Log.e(
                "Firebase Image URL",
                taskSnapshot.metadata!!.reference!!.downloadUrl.toString()
            )

            //Get downloadable url from the task snapshot
            taskSnapshot.metadata!!.reference!!.downloadUrl
                .addOnSuccessListener { uri ->
                    Log.e("Downloadable Image URL", uri.toString())
                    when(activity){
                        is UserProfileActivity -> {
                            activity.imageUploadSuccess(uri.toString())
                        }
                    }
                }
        }
            .addOnFailureListener{ exception ->
                when(activity){
                    is UserProfileActivity -> {
                        activity.hideProgressDialog()
                    }
                }

                Log.e(
                    activity.javaClass.simpleName,
                    exception.message,
                    exception
                )
            }
    }*/
}