package com.magdalenaknezevic.loginapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.magdalenaknezevic.loginapp.R
import com.magdalenaknezevic.loginapp.models.Order
import com.magdalenaknezevic.loginapp.models.Product

class OrdersAdapter(private var items: ArrayList<Order>,
                    private val listener: OrdersAdapter.ContentListener
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrdersAdapter.OrdersViewHolder {
        return OrdersAdapter.OrdersViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(
                    R.layout.recycler_orders, parent, false
                )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is OrdersAdapter.OrdersViewHolder -> {
                holder.bind(listener, items[position])
            }
        }

    }

    override fun getItemCount(): Int {
        return items.size
    }

    class OrdersViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        private val db = Firebase.firestore

        private val productImage: ImageView =
            itemView.findViewById(R.id.ivImageOfProduct)
        private val productName: TextView =
            itemView.findViewById(R.id.tvName)
        private val productCategory: TextView =
            itemView.findViewById(R.id.tvCategoryOfProduct)
        // private val deleteButton = view.findViewById<ImageView>(R.id.ivDelete)

        fun bind(
            listener: OrdersAdapter.ContentListener,
            order: Order,
            //index: Int

        ) {
            Glide.with(itemView.context).load(order.image).into(productImage)
            productName.text = order.name
            productCategory.text = order.category

            productImage.setOnClickListener {
                listener.onItemButtonClick(order)
            }

        }
    }

    interface ContentListener {
        fun onItemButtonClick(order: Order)
    }


}