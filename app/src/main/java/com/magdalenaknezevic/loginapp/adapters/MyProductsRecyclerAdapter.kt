package com.magdalenaknezevic.loginapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.magdalenaknezevic.loginapp.R
import com.magdalenaknezevic.loginapp.models.Product


class MyProductsRecyclerAdapter(
    private var items: ArrayList<Product>,
    private val listener: ContentListener
    ): RecyclerView.Adapter<RecyclerView.ViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyProductsViewHolder {
        return MyProductsViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(
                R.layout.recycler_products, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is MyProductsViewHolder -> {
                holder.bind(listener, items[position])
            }
        }

    }

    override fun getItemCount(): Int {
        return items.size
    }


    class MyProductsViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        private val db = Firebase.firestore

        private val productImage: ImageView =
            itemView.findViewById(R.id.ivImageOfProduct)
        private val productName: TextView =
            itemView.findViewById(R.id.tvName)
        private val productCategory: TextView =
            itemView.findViewById(R.id.tvCategoryOfProduct)
       // private val deleteButton = view.findViewById<ImageView>(R.id.ivDelete)

        fun bind(
            listener: ContentListener,
            product: Product,
            //index: Int

        ) {
            Glide.with(itemView.context).load(product.image).into(productImage)
            productName.text = product.name
            productCategory.text = product.category

            productImage.setOnClickListener {
                listener.onItemButtonClick(product)
            }

        }
    }

    interface ContentListener {
        fun onItemButtonClick(product: Product)
    }
}