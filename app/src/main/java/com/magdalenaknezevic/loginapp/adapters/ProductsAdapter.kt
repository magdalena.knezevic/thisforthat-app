package com.magdalenaknezevic.loginapp.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.View
import android.widget.TextView
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.magdalenaknezevic.loginapp.R
import com.magdalenaknezevic.loginapp.models.Product


class ProductsAdapter (private var items : ArrayList<Product>,
                       private val listener: ContentListener
): RecyclerView.Adapter<RecyclerView.ViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsViewHolder {
        return ProductsViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.recycler_orders, parent, false)

        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder) {
            is ProductsViewHolder -> {
                holder.bind(listener, items[position])
            }
        }
    }

    override fun getItemCount(): Int {
        return this.items.size
    }

    fun searchDataList(searchList: ArrayList<Product>){
        items = searchList
        notifyDataSetChanged()

    }

    class ProductsViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
/*
        private val productImage = itemView.findViewById<ImageView>(R.id.ivImageOfProduct)
        private val productName = itemView.findViewById<TextView>(R.id.tvName)
        private val productCategory = itemView.findViewById<TextView>(R.id.tvCategoryOfProduct)

        fun bind(listener: ContentListener, products: Product){
            Glide.with(itemView.context).load(products.image).into(productImage)
            productName.text = products.name
            productCategory.text = products.category
            }
        */
        private val db = Firebase.firestore
        private val productImage: ImageView =
            itemView.findViewById(R.id.ivImageOfProduct)
        private val productName: TextView =
            itemView.findViewById(R.id.tvName)
        private val productCategory: TextView =
            itemView.findViewById(R.id.tvCategoryOfProduct)

        fun bind(
            listener: ContentListener,
            product: Product
        ) {
            Glide.with(itemView.context).load(product.image).into(productImage)
            productName.text = product.name
            productCategory.text = product.category

            productImage.setOnClickListener {
                listener.onItemButtonClick(product)
            }
        }
    }



    interface ContentListener {
        fun onItemButtonClick(product: Product)
    }
}


