package com.magdalenaknezevic.loginapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.magdalenaknezevic.loginapp.R
import com.magdalenaknezevic.loginapp.models.Product

class RequestSelectAdapter(
    private val items : ArrayList<Product>,
    private val listener: ContentListener
): RecyclerView.Adapter<RecyclerView.ViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RequestSelectViewHolder {
        return RequestSelectViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.recycler_request, parent, false)

        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder) {
            is RequestSelectViewHolder -> {
                holder.bind(listener, items[position])
            }
        }
    }

    override fun getItemCount(): Int {
        return this.items.size
    }

    class RequestSelectViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        private val db = Firebase.firestore
        private val productImage: ImageView =
            itemView.findViewById(R.id.ivImageRequest)
        private val productName: TextView =
            itemView.findViewById(R.id.tvNameRequest)
        private val selectButton: Button =
            itemView.findViewById(R.id.bttnSelectRequest)

        fun bind(
            listener: ContentListener,
            product: Product
        ) {
            Glide.with(itemView.context).load(product.image).into(productImage)
            productName.text = product.name

            selectButton.setOnClickListener {
                listener.onItemButtonClick(product)
            }
        }
    }

    interface ContentListener {
        fun onItemButtonClick(product: Product)
    }
}
