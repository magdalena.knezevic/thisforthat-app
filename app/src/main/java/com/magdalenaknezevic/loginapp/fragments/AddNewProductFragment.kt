package com.magdalenaknezevic.loginapp.fragments

import android.app.ActionBar
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.magdalenaknezevic.loginapp.R
import com.magdalenaknezevic.loginapp.activites.MainActivity
import com.magdalenaknezevic.loginapp.models.Product
import com.magdalenaknezevic.loginapp.utils.Constants

class AddNewProductFragment : Fragment() {

    private lateinit var firebaseAuth: FirebaseAuth
    private val db = Firebase.firestore

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_add_new_product, container, false)

        firebaseAuth = FirebaseAuth.getInstance()
        ActionBar.DISPLAY_SHOW_TITLE
        //((MainActivity) getActivity()).getSupportActionBar().setTitle("Add new product")
        //activity.title("Add new Product")
        activity?.setTitle("Add new product")
        //activity

        val backButton = view.findViewById<ImageView>(R.id.ivBack)
        val enterImageURL = view.findViewById<EditText>(R.id.imageEt)
        val enterCategory = view.findViewById<EditText>(R.id.categoryEt)
        val enterName = view.findViewById<EditText>(R.id.nameEt)
        val enterDescription = view.findViewById<EditText>(R.id.productDescriptionEt)
        val uploadButton = view.findViewById<Button>(R.id.buttonRequest)


        backButton.setOnClickListener {
            val intent =
                Intent(this@AddNewProductFragment.requireContext(), MainActivity::class.java)
            intent.putExtra(Constants.EXTRA_USER_DETAILS, (activity as MainActivity).userDetails)
            startActivity(intent)
        }

        uploadButton.setOnClickListener {
            val name = enterName.text.toString()
            val category = enterCategory.text.toString()
            val description = enterDescription.text.toString()
            val image = enterImageURL.text.toString()

            fun generateKeywords(name: String): List<String> {
                val keywords = mutableListOf<String>()
                for (i in 0 until name.length) {
                    for (j in (i+1)..name.length) {
                        keywords.add(name.slice(i until j))
                    }
                }
                return keywords
            }
            if (name.isNotEmpty() && category.isNotEmpty() && description.isNotEmpty()) {
                //showProgressDialog("Please Wait...")
                val user = firebaseAuth.currentUser
                val id = user!!.uid
                val product = Product(
                    name = name,
                    category = category,
                    description = description,
                    ownerID = id,
                    image = image
                )

                val newProduct = db.collection("products").document()
                product.id = newProduct.id
                db.collection("products").document(product.id).set(product)

                val intent =
                    Intent(this@AddNewProductFragment.requireContext(), MainActivity::class.java)
                startActivity(intent)

            } else {
                Toast.makeText(
                    activity?.baseContext,
                    "Empty Fields Are Not Allowed",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
         return view
    }
}



    /*fun addProductSuccess(){

        hideProgressDialog()

        Toast.makeText(this@AddNewProductActivity,
            resources.getString(R.string.add_new_product),
            Toast.LENGTH_SHORT
        ).show()
    }*/

    //fun productAddedSuccess(product: Product){
    //    hideProgressDialog()

        //if(product.productCompleted == 0){
    //    Toast.makeText(this@AddNewProductActivity, "Unable to upload new product", Toast.LENGTH_LONG).show()

        //} else {
    //    startActivity(Intent(this@AddNewProductActivity, MainActivity::class.java))
        //}
    //    finish()
    //}
