package com.magdalenaknezevic.loginapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.magdalenaknezevic.loginapp.R
//import com.magdalenaknezevic.loginapp.activites.AddNewProductActivity
import com.magdalenaknezevic.loginapp.models.Product
import com.magdalenaknezevic.loginapp.adapters.MyProductsRecyclerAdapter

class MyProductsFragment : Fragment(),
    MyProductsRecyclerAdapter.ContentListener {
    private lateinit var firebaseAuth: FirebaseAuth
    private val db = Firebase.firestore
    private lateinit var recyclerAdapter: MyProductsRecyclerAdapter
    //lateinit var recyclerView: RecyclerView
    //lateinit var product: Product
    //lateinit var productList: ArrayList<Product>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_my_products, container, false)
        firebaseAuth = FirebaseAuth.getInstance()
        val user = firebaseAuth.currentUser
        val id = user!!.uid
        val recyclerView = view.findViewById<RecyclerView>(R.id.recycler_products)
        db.collection("products")
            .whereEqualTo("ownerID", id)
            .get()
            .addOnSuccessListener { result ->
                val products = ArrayList<Product>()
                for (data in result.documents) {
                    val product = data.toObject(Product::class.java)
                    products.add(product!!)

                    recyclerAdapter = MyProductsRecyclerAdapter(products, this@MyProductsFragment)
                    recyclerView.apply {
                        layoutManager = LinearLayoutManager(context)
                        adapter = recyclerAdapter
                    }
                }
            }
        /*fun initView() {
            view?.findViewById<RecyclerView>(R.id.recycler_products)?.apply {
                layoutManager =
                    LinearLayoutManager(context)
                adapter =
                    productsAdapter
            }
        }*/
        /*recyclerView = RecyclerView(this@MyProductsFragment.requireContext())
        product = Product()
        productList = arrayListOf()*/

        /*productsAdapter = MyProductsRecyclerAdapter(productList)

        recyclerView.adapter = productsAdapter

        EventChangeListener()*/

        /*db = FirebaseFirestore.getInstance()

        FirestoreClass().getProductDetails(AddNewProductActivity())

        /*val productName = view.findViewById<TextView>(R.id.recycler_products)
        val productCategory = view.findViewById<TextView>(R.id.tvCategoryOfProduct)

        val sharedPreferences = this.getActivity()?.getSharedPreferences(Constants.THISFORTHAT_PREFERENCES, Context.MODE_PRIVATE)

        val name = sharedPreferences?.getString(Constants.PRODUCT_NAME, "")
        val category = sharedPreferences?.getString(Constants.PRODUCT_CATEGORY, "")
        productName.text = "$name"
        //productCategory.text = "$category"

        product = Product(
            productName.toString(),
           // productCategory.toString()
        )

        fun setupData(): ArrayList<Product> {
            val list = ArrayList<Product>()
            list.add(
                product
                )
            return list

        }

        productsAdapter.postItemsList(setupData())
        initView()*/*/


        return view
    }

    override fun onItemButtonClick(product: Product) {

        val detailsFragment = MyProductsDetailsFragment()
        //val studentEmail = arguments?.getString("STUDENT_EMAIL").toString()
        val bundle = Bundle()
        bundle.putString("product_id", product.id)
        detailsFragment.arguments = bundle

        val fragmentTransaction: FragmentTransaction? =
            activity?.supportFragmentManager?.beginTransaction()
        fragmentTransaction?.replace(
            R.id.fragment_container,
            detailsFragment)
        fragmentTransaction?.commit()


    }

        /*private fun EventChangeListener(){

        db = FirebaseFirestore.getInstance()
        db.collection(Constants.PRODUCTS)
            .addSnapshotListener(object : EventListener<QuerySnapshot> {
                override fun onEvent(
                    value: QuerySnapshot?,
                    error: FirebaseFirestoreException?
                ) {
                    if(error != null){
                        Log.e("Firestore error", error.message.toString())
                        return
                    }
                    for(dc: DocumentChange in value?.documentChanges!!){
                        if(dc.type == DocumentChange.Type.ADDED){
                            productList.add(dc.document.toObject(Product::class.java))
                        }
                    }
                    productsAdapter.notifyDataSetChanged()
                }
            })
    }*/
    }
