package com.magdalenaknezevic.loginapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import com.bumptech.glide.Glide
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.magdalenaknezevic.loginapp.R
import com.magdalenaknezevic.loginapp.models.Product


class ProductDetailsFragment : Fragment() {

    //private lateinit var firebaseAuth: FirebaseAuth
    private val db = Firebase.firestore

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_product_details, container, false)


        val image = view.findViewById<ImageView>(R.id.ivProductImage)
        val category = view.findViewById<TextView>(R.id.tvCategory)
        val description = view.findViewById<TextView>(R.id.tvProductDescription)
        val name = view.findViewById<TextView>(R.id.tvProductName)
        val button = view.findViewById<AppCompatButton>(R.id.buttonRequest)

        val productId = arguments?.getString("product_id").toString()

        val bundle = Bundle()
        bundle.putString("product_idd", productId)
        RequestSelectFragment().arguments = bundle

        db.collection("products")
            .document(productId)
            .get()
            .addOnSuccessListener { result ->
                val product = result.toObject(Product::class.java)!!

                Glide.with(view.context).load(product.image).into(image)
                name.text = product.name
                category.text = product.category
                description.text = product.description
            }

        button.setOnClickListener {
                val productId = arguments?.getString("product_id").toString()

                // Retrieve the image and name from Firestore based on the productId
                db.collection("products")
                    .document(productId)
                    .get()
                    .addOnSuccessListener { document ->
                        val image = document.getString("image")
                        val name = document.getString("name")
                        val ownerID = document.getString("ownerID")
                        val desc = document.getString("description")
                        val category = document.getString("category")

                        val bundle = Bundle().apply {
                            putString("image", image) // Replace "image" with the appropriate key
                            putString("name", name) // Replace "name" with the appropriate key
                            putString("ownerID", ownerID)
                            putString("description", desc)
                            putString("category", category)
                        }

                        val requestSelectFragment = RequestSelectFragment()
                        requestSelectFragment.arguments = bundle

                        val requestFragment = RequestFragment()
                        requestFragment.arguments = bundle





                        // Navigate to the RequestSelectFragment
                        // Replace the code below with your actual navigation logic
                        val transaction = requireActivity().supportFragmentManager.beginTransaction()
                        transaction.replace(R.id.fragment_container, requestSelectFragment)
                        transaction.addToBackStack(null)


                        //transaction.replace(R.id.fragment_container, requestFragment)
                        //transaction.addToBackStack(null)
                        transaction.commit()

                    }
            }
        return view
    }

}
