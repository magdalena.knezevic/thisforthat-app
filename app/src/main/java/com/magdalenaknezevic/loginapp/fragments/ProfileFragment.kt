package com.magdalenaknezevic.loginapp.fragments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.magdalenaknezevic.loginapp.R
import com.magdalenaknezevic.loginapp.activites.*
import com.magdalenaknezevic.loginapp.databinding.FragmentProfileBinding
import com.magdalenaknezevic.loginapp.models.Product
import com.magdalenaknezevic.loginapp.models.User
import com.magdalenaknezevic.loginapp.utils.Constants

open class ProfileFragment : Fragment(){

    private lateinit var navigationView: NavigationView
    private lateinit var binding: FragmentProfileBinding
    lateinit var user: User

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_profile, container, false)

        navigationView = view.findViewById(R.id.nav_profile)
        user = User()

        val bundle = arguments

       /* var edit = view.findViewById<TextView>(R.id.tv_edit)
         edit.setOnClickListener{
             val intent = Intent(this@ProfileFragment.requireContext(), UserProfileActivity::class.java)
             //intent.putExtra(Constants.EXTRA_USER_DETAILS, (activity as MainActivity).userDetails)
             //startActivity(intent)
         }*/

        val nameProfile = view.findViewById<TextView>(R.id.tv_name)
        val emailProfile = view.findViewById<TextView>(R.id.tv_email)
        //val tvLastName = view.findViewById<TextView>(R.id.tv_lastName)
        val tvAddress = view.findViewById<TextView>(R.id.tv_address)

        val db = Firebase.firestore
        val uid = FirebaseAuth.getInstance().currentUser?.uid

        if (uid != null) {
            db.collection("users").document(uid)
                .get()
                .addOnSuccessListener { document ->
                    if (document != null && document.exists()) {
                        // Document exists, retrieve attributes
                        val name = document.getString("firstName")
                        val email = document.getString("email")
                        val lastName = document.getString("lastName")
                        val address = document.getString("address")
                        // Process the retrieved attributes as needed
                        val combined = "$name  $lastName"
                        nameProfile.text = combined
                        //nameProfile.text = lastName
                        emailProfile.text=email
                        //tvLastName.text = lastName
                        tvAddress.text= address
                    } else {
                        // Document doesn't exist
                    }
                }
                .addOnFailureListener { exception ->
                    // Error occurred while fetching the document
                    // Handle the fa
                    Log.e("Firestore Error", "Error")
                }
        }



        val sharedPreferences = this.getActivity()?.getSharedPreferences(Constants.THISFORTHAT_PREFERENCES, Context.MODE_PRIVATE)
        val username = sharedPreferences?.getString(Constants.LOGGED_IN_USERNAME, "")!!
        val email = sharedPreferences?.getString(Constants.LOGGED_IN_EMAIL, "")
        //nameProfile.text = username
        //emailProfile.text = "Your email adress:\n" + user.email

        navigationView.setNavigationItemSelectedListener{

                it.isChecked = true

                when(it.itemId){
                    R.id.profile_products -> replaceFragments(MyProductsFragment(), it.title.toString())
                    R.id.profile_orders -> replaceFragments(OrdersFragment(), it.title.toString())
                    R.id.profile_info -> replaceFragments(AboutTheAppFragment(), it.title.toString())

                }
                true
            }

        return view
    }

    private fun replaceFragments(fragment: Fragment, title: String){
        val fragmentTransaction: FragmentTransaction? =
            activity?.supportFragmentManager?.beginTransaction()
        fragmentTransaction?.replace(R.id.fragment_container, fragment)
        fragmentTransaction?.commit()
        activity?.setTitle(title)
    }
}

