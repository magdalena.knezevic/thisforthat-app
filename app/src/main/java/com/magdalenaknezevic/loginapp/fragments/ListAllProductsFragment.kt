package com.magdalenaknezevic.loginapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.magdalenaknezevic.loginapp.R
import com.magdalenaknezevic.loginapp.activites.find
import com.magdalenaknezevic.loginapp.adapters.MyProductsRecyclerAdapter
import com.magdalenaknezevic.loginapp.models.Product
import com.magdalenaknezevic.loginapp.adapters.ProductsAdapter
import com.magdalenaknezevic.loginapp.databinding.ActivityMainBinding

class ListAllProductsFragment : Fragment(),
    ProductsAdapter.ContentListener {
    private lateinit var firebaseAuth: FirebaseAuth
    val db = Firebase.firestore
    private lateinit var binding : ActivityMainBinding
    private lateinit var recyclerAdapter: ProductsAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_list_all_products, container, false)
        //lateinit var recyclerView: RecyclerView
        //lateinit var productsList: ArrayList<Product>
        //lateinit var firebaseAuth: FirebaseAuth
        //val user = firebaseAuth.currentUser
        //val id = user!!.uid
        val recyclerView = view.findViewById<RecyclerView>(R.id.recycler_list)
        activity?.setTitle("Search for Products")
        firebaseAuth = FirebaseAuth.getInstance()
        val user = firebaseAuth.currentUser
        val id = user!!.uid


        /*var backButton = view.findViewById<ImageView>(R.id.ivBackButton)

        backButton.setOnClickListener {
            val sureFragment = HomeFragment()

            val fragmentTransaction: FragmentTransaction? =
                activity?.supportFragmentManager?.beginTransaction()
            fragmentTransaction?.replace(R.id.fragment_container, HomeFragment())
            fragmentTransaction?.commit()
        }*/


        val inputText = view.findViewById<EditText>(R.id.etProduct)
        val inputSearch = view.findViewById<ImageView>(R.id.ivProduct)


        inputSearch.setOnClickListener {
            val textForSearch = inputText.text.toString()

            if (textForSearch.isNotBlank()) {
                db.collection("products")
                    .whereEqualTo("name", textForSearch)
                    .get()
                    .addOnSuccessListener { result ->
                        val products = ArrayList<Product>()
                        for (data in result.documents) {
                            val product = data.toObject(Product::class.java)
                            product?.let {
                                if (it.ownerID != id) {
                                    products.add(it)
                                }
                            }
                        }

                        if (products.isNotEmpty()) {
                            recyclerAdapter = ProductsAdapter(products, this@ListAllProductsFragment)
                            recyclerView.apply {
                                layoutManager = LinearLayoutManager(context)
                                adapter = recyclerAdapter
                            }
                        } else {
                            Toast.makeText(
                                activity?.baseContext,
                                "No match found.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    .addOnFailureListener { exception ->
                        Toast.makeText(
                            activity?.baseContext,
                            "Error retrieving products: ${exception.message}",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
            } else {
                Toast.makeText(
                    activity?.baseContext,
                    "You searched for an empty product.",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }


        /* val service = ServiceBuilder.buildService(FakerEndPoints::class.java)

         val call = service.getProducts(10)
         call.enqueue(object : Callback<ArrayList<Product>> {
             override fun onResponse(
                 call: Call<ArrayList<Product>>,
                 response: Response<ArrayList<Product>>
             ) {
                 if (response.isSuccessful) {
                     view?.findViewById<RecyclerView>(R.id.recycler_list)?.apply {
                         layoutManager =
                             LinearLayoutManager(context)
                         adapter =
                             ProductsAdapter(response.body()!!)
                     }
                 }
             }

             override fun onFailure(call: Call<ArrayList<Product>>, t: Throwable) {
                 Log.e("ListAllProductsFragment", t.message.toString())
             }
         }
         ) */

        return view
    }

    override fun onItemButtonClick(product: Product) {
        val detailsFragment = ProductDetailsFragment()
        val bundle = Bundle()
        bundle.putString("product_id", product.id)
        detailsFragment.arguments = bundle

        val fragmentTransaction: FragmentTransaction? =
            activity?.supportFragmentManager?.beginTransaction()
        fragmentTransaction?.replace(
            R.id.fragment_container,
            detailsFragment)
        fragmentTransaction?.commit()
    }
}

