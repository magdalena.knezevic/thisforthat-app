package com.magdalenaknezevic.loginapp.fragments

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.FragmentTransaction
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.magdalenaknezevic.loginapp.R
import com.magdalenaknezevic.loginapp.activites.SignInActivity
import com.magdalenaknezevic.loginapp.activites.find
import com.magdalenaknezevic.loginapp.databinding.ActivityMainBinding
import com.magdalenaknezevic.loginapp.models.Order
import com.magdalenaknezevic.loginapp.models.Product

class RequestFragment : Fragment(){
    private lateinit var firebaseAuth: FirebaseAuth
    private val db = Firebase.firestore
    private lateinit var binding : ActivityMainBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_request, container, false)
        val requestBttn = view.findViewById<AppCompatButton>(R.id.buttonRequest)

        firebaseAuth = FirebaseAuth.getInstance()
        val user = firebaseAuth.currentUser
        val id = user!!.uid
        activity?.setTitle("Request")

        val productImg = view.findViewById<ImageView>(R.id.ivThat)
        val productName = view.findViewById<TextView>(R.id.tvThat)

        val productImgThis = view.findViewById<ImageView>(R.id.ivThis)
        val productNameThis = view.findViewById<TextView>(R.id.tvThis)

        val image = arguments?.getString("imageP")
        val name = arguments?.getString("nameP")

        if (!image.isNullOrEmpty()) {
            Glide.with(this)
                .load(image)
                .into(productImg)
        }

        productName.text = name

        val imageThis = arguments?.getString("image")
        val nameThis = arguments?.getString("name")
        val ownerID = arguments?.getString("ownerID")
        val category = arguments?.getString("category")
        val desc = arguments?.getString("description")

        if (!imageThis.isNullOrEmpty()) {
            Glide.with(this)
                .load(imageThis)
                .into(productImgThis)
        }

        productNameThis.text = nameThis

       // productName.text = name

        /*val bundle = Bundle()
        bundle.putString("owner_id", ownerID)
        //EmailFragment().arguments = bundle
        val emailFragment = EmailFragment()
        emailFragment.arguments = bundle*/

        val order = Order(
            name = nameThis!!,
            category = category!!,
            description = desc!!,
            customerID = id,
            image = imageThis!!
        )


        requestBttn.setOnClickListener {

            view.apply {
                val builder = AlertDialog.Builder(context)

                builder.setTitle("Confirm")
                builder.setMessage("When you press this button, your order will be shown in Orders section. After that, you will be redirected to Gmail to arrange the terms of order with the owner.  Are you sure you want to place the order ? ")

                builder.setPositiveButton("YES") { dialog, which -> // Do nothing but close the dialog
                    val newOrder = db.collection("orders").document()
                    order.id = newOrder.id
                    db.collection("orders").document(order.id!!).set(order)
                    val bundle = Bundle()
                    bundle.putString("owner_id", ownerID)
                    //EmailFragment().arguments = bundle
                    val emailFragment = EmailFragment()
                    emailFragment.arguments = bundle

                    val transaction = requireActivity().supportFragmentManager.beginTransaction()
                    transaction.replace(R.id.fragment_container, emailFragment)
                    transaction.addToBackStack(null)
                    transaction.commit()
                    activity?.setTitle("Send email")

                }

                builder.setNegativeButton(
                    "NO"
                ) { dialog, which -> // Do nothing
                    dialog.dismiss()
                }

                val alert = builder.create()
                alert.show()

            }

        }


            return view
        }
}