package com.magdalenaknezevic.loginapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import com.bumptech.glide.Glide
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.magdalenaknezevic.loginapp.R
import com.magdalenaknezevic.loginapp.models.Product


class MyProductsDetailsFragment : Fragment() {

    private val db = Firebase.firestore

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_my_products_details, container, false)

        val image = view.findViewById<ImageView>(R.id.ivMyProductImage)
        val category = view.findViewById<TextView>(R.id.tvMyCategory)
        val description = view.findViewById<TextView>(R.id.tvMyProductDescription)
        val name = view.findViewById<TextView>(R.id.tvMyProductName)

        val productId = arguments?.getString("product_id").toString()

        db.collection("products")
            .document(productId)
            .get()
            .addOnSuccessListener { result ->
                val product = result.toObject(Product::class.java)!!

                Glide.with(view.context).load(product.image).into(image)
                name.text = product.name
                category.text = product.category
                description.text = product.description
            }

        return view
    }


}