package com.magdalenaknezevic.loginapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.magdalenaknezevic.loginapp.R
import com.magdalenaknezevic.loginapp.models.Order
import com.magdalenaknezevic.loginapp.models.Product

class OrdersDetailsFragment : Fragment() {

    private val db = Firebase.firestore

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_orders_details, container, false)


        val image = view.findViewById<ImageView>(R.id.ivOrderImage)
        val category = view.findViewById<TextView>(R.id.tvOrderCategory)
        val description = view.findViewById<TextView>(R.id.tvOrderDescription)
        val name = view.findViewById<TextView>(R.id.tvOrderName)

        val orderId = arguments?.getString("order_id").toString()

        db.collection("orders")
            .document(orderId)
            .get()
            .addOnSuccessListener { result ->
                val order = result.toObject(Order::class.java)!!

                Glide.with(view.context).load(order.image).into(image)
                name.text = order.name
                category.text = order.category
                description.text = order.description
            }




        return view
    }


}