package com.magdalenaknezevic.loginapp.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.magdalenaknezevic.loginapp.R
import com.magdalenaknezevic.loginapp.activites.find
import com.magdalenaknezevic.loginapp.adapters.RequestSelectAdapter
import com.magdalenaknezevic.loginapp.databinding.ActivityMainBinding
import com.magdalenaknezevic.loginapp.models.Product

class EmailFragment : Fragment() {

    private val db = Firebase.firestore
    private lateinit var binding : ActivityMainBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_email, container, false)
        val ownersEmail = view.findViewById<TextView>(R.id.tv_ownersEmail)

        val ownerID = arguments?.getString("owner_id")
        //ownersEmail.text = ownerID


        // Retrieve the image and name from Firestore based on the productId
        db.collection("users")
            .document(ownerID!!)
            .get()
            .addOnSuccessListener { document ->
                val email = document.getString("email")
                ownersEmail.text = email

            }
            .addOnFailureListener {
                Log.e("Firestore", "Error getting document: it.exception")
            }

        return view
    }

}