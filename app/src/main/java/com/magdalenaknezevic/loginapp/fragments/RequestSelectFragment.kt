package com.magdalenaknezevic.loginapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.magdalenaknezevic.loginapp.R
import com.magdalenaknezevic.loginapp.adapters.MyProductsRecyclerAdapter
import com.magdalenaknezevic.loginapp.adapters.ProductsAdapter
import com.magdalenaknezevic.loginapp.adapters.RequestSelectAdapter
import com.magdalenaknezevic.loginapp.databinding.ActivityMainBinding
import com.magdalenaknezevic.loginapp.models.Product

class RequestSelectFragment : Fragment(),
    RequestSelectAdapter.ContentListener  {
    private lateinit var firebaseAuth: FirebaseAuth
    private val db = Firebase.firestore
    private lateinit var recyclerAdapter: RequestSelectAdapter
    private lateinit var binding : ActivityMainBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_request_select, container, false)

        firebaseAuth = FirebaseAuth.getInstance()
        val user = firebaseAuth.currentUser
        val id = user!!.uid

        val productImg = view.findViewById<ImageView>(R.id.ivRequestThis)
        val productName = view.findViewById<TextView>(R.id.tvNameRequestThis)
        val recyclerView = view.findViewById<RecyclerView>(R.id.rvRequest)

        //val productIdRequest = arguments?.getString("product_id_request").toString()

        val image = arguments?.getString("image")
        val name = arguments?.getString("name")

        if (!image.isNullOrEmpty()) {
            Glide.with(this)
                .load(image)
                .into(productImg)
        }

        productName.text = name

        /*val testName = productName.toString()
        val testImg = productImg.toString()*/

        //val image = document.getString("image")
        //val name = document.getString("name")

       /* val bundle = Bundle().apply {
            putString("imageThis", testImg) // Replace "image" with the appropriate key
            putString("nameThis", testName) // Replace "name" with the appropriate key
        }

        val requestFragment = RequestFragment()
        requestFragment.arguments = bundle*/


        db.collection("products")
            .whereEqualTo("ownerID", id)
            .get()
            .addOnSuccessListener { result ->
                val products = ArrayList<Product>()
                for (data in result.documents) {
                    val product = data.toObject(Product::class.java)
                    products.add(product!!)

                    recyclerAdapter = RequestSelectAdapter(products, this@RequestSelectFragment)
                    recyclerView.apply {
                        layoutManager = LinearLayoutManager(context)
                        adapter = recyclerAdapter
                    }
                }

            }

        return view
    }
    override fun onItemButtonClick(product: Product) {
        val image = arguments?.getString("image")
        val name = arguments?.getString("name")
        val ownerID = arguments?.getString("ownerID")
        val desc = arguments?.getString("description")
        val category = arguments?.getString("category")
        //val productId = product.id
        val productName = product.name
        val productImage = product.image

        val bundle = Bundle()
        bundle.putString("imageP", productImage)
        bundle.putString("nameP", productName)

        bundle.putString("image", image)
        bundle.putString("name", name)
        bundle.putString("ownerID", ownerID)
        bundle.putString("description", desc)
        bundle.putString("category", category)

        val requestFragment = RequestFragment()
        requestFragment.arguments = bundle

        // Navigate to the RequestSelectFragment
        // Replace the code below with your actual navigation logic
        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, requestFragment)
        transaction.addToBackStack(null)
        transaction.commit()
        // Retrieve the image and name from Firestore based on the productId
        /*db.collection("products")
            .document(productId)
            .get()
            .addOnSuccessListener { document ->
                val image = document.getString("image")
                val name = document.getString("name")

                val bundle = Bundle().apply {
                    putString("imageP", image) // Replace "image" with the appropriate key
                    putString("nameP", name) // Replace "name" with the appropriate key
                }

                val requestFragment = RequestFragment()
                requestFragment.arguments = bundle

                // Navigate to the RequestSelectFragment
                // Replace the code below with your actual navigation logic
                val transaction = requireActivity().supportFragmentManager.beginTransaction()
                transaction.replace(R.id.fragment_container, requestFragment)
                transaction.addToBackStack(null)
                transaction.commit()

    }*/

    }

}