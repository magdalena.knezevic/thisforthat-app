package com.magdalenaknezevic.loginapp.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.magdalenaknezevic.loginapp.R
//import com.magdalenaknezevic.loginapp.activites.AddNewProductActivity
import com.magdalenaknezevic.loginapp.activites.BaseActivity
import com.magdalenaknezevic.loginapp.utils.Constants

class HomeFragment : Fragment() {

    //fun onCreate - poziva se kada je fragment stvoren
    //fun onCreateView - poziva se kada je View kreiran
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        val addNewProductFragment = AddNewProductFragment()
        val listAllProductsFragment = ListAllProductsFragment()

        val etAdd = view.findViewById<TextView>(R.id.etAdd)
        val ivAdd = view.findViewById<ImageView>(R.id.ivAdd)
        val clothes = view.findViewById<ImageView>(R.id.clothes)
        val music = view.findViewById<ImageView>(R.id.music)
        val technology = view.findViewById<ImageView>(R.id.technology)
        val toys = view.findViewById<ImageView>(R.id.toys)
        val furniture = view.findViewById<ImageView>(R.id.furniture)
        val whiteTech = view.findViewById<ImageView>(R.id.whiteTech)

        var searchButton = view.findViewById<ImageView>(R.id.ivSearch)

        fun replaceFragments(fragment: Fragment){
            val fragmentTransaction: FragmentTransaction? =
                activity?.supportFragmentManager?.beginTransaction()
            fragmentTransaction?.replace(R.id.fragment_container, fragment)
            fragmentTransaction?.commit()
        }

        searchButton.setOnClickListener{
            replaceFragments(ListAllProductsFragment())
        }
        // searchButton = findViewById<ImageView>(R.id.ivSearch)

        //searchButton.setOnClickListener{
        //    replaceFragments(ListAllProductsFragment(), "All Products")
        // }



        var addButton = view.findViewById<ImageView>(R.id.ivAdd)
        addButton.setOnClickListener{
            replaceFragments(AddNewProductFragment())
        }
        return view
    }

}



