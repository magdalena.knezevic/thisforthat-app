package com.magdalenaknezevic.loginapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.magdalenaknezevic.loginapp.R
import com.magdalenaknezevic.loginapp.adapters.MyProductsRecyclerAdapter
import com.magdalenaknezevic.loginapp.adapters.OrdersAdapter
import com.magdalenaknezevic.loginapp.models.Order
import com.magdalenaknezevic.loginapp.models.Product
import com.magdalenaknezevic.loginapp.utils.FakerEndPoints
import com.magdalenaknezevic.loginapp.utils.ServiceBuilder

class OrdersFragment : Fragment(),
    OrdersAdapter.ContentListener {

    private lateinit var firebaseAuth: FirebaseAuth
    private val db = Firebase.firestore
    private lateinit var ordersAdapter: OrdersAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_orders, container, false)

        firebaseAuth = FirebaseAuth.getInstance()
        val user = firebaseAuth.currentUser
        val id = user!!.uid
        val recyclerView = view.findViewById<RecyclerView>(R.id.recycler_orders)

        db.collection("orders")
            .whereEqualTo("customerID", id)
            .get()
            .addOnSuccessListener { result ->
                val orders = ArrayList<Order>()
                for (data in result.documents) {
                    val order = data.toObject(Order::class.java)
                    orders.add(order!!)

                    ordersAdapter = OrdersAdapter(orders, this@OrdersFragment)
                    recyclerView.apply {
                        layoutManager = LinearLayoutManager(context)
                        adapter = ordersAdapter
                    }
                }
            }

        return view
    }

    override fun onItemButtonClick(order: Order) {
        val ordersDetailsFragment = OrdersDetailsFragment()
        //val studentEmail = arguments?.getString("STUDENT_EMAIL").toString()
        val bundle = Bundle()
        bundle.putString("order_id", order.id)
        ordersDetailsFragment.arguments = bundle

        val fragmentTransaction: FragmentTransaction? =
            activity?.supportFragmentManager?.beginTransaction()
        fragmentTransaction?.replace(
            R.id.fragment_container,
            ordersDetailsFragment)
        fragmentTransaction?.commit()
    }

}