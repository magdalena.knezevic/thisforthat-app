package com.magdalenaknezevic.loginapp.fragments

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.magdalenaknezevic.loginapp.R
import com.magdalenaknezevic.loginapp.activites.SignInActivity


class LogOutFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_log_out, container, false)

        view.apply {
            val builder = AlertDialog.Builder(context)

            builder.setTitle("Confirm")
            builder.setMessage("Are you sure you want to Exit?")

            builder.setPositiveButton("YES") { dialog, which -> // Do nothing but close the dialog
                //dialog.dismiss()
                FirebaseAuth.getInstance().signOut()
                val intent = Intent(this@LogOutFragment.requireContext(), SignInActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }

            builder.setNegativeButton(
                "NO"
            ) { dialog, which -> // Do nothing
                dialog.dismiss()
            }

            val alert = builder.create()
            alert.show()

        }


        return view
    }


}