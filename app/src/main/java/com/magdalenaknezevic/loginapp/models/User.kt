package com.magdalenaknezevic.loginapp.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    val id: String? = "",
    val firstName: String? = "",
    val lastName: String? = "",
    val password: String? = "",
    val email: String? = "",
    val mobile: String? = "",
    val gender: String? = "",
    val address: String? = "",
): Parcelable
