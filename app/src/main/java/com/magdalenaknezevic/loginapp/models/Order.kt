package com.magdalenaknezevic.loginapp.models

data class Order(
    var id: String? = "",
    var name: String = "",
    var category: String = "",
    var description: String ="",
    val customerID: String = "",
    val image: String = "",
)
