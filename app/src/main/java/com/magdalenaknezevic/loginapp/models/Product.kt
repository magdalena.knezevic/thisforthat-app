package com.magdalenaknezevic.loginapp.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Product(
    var id: String = "",
    var name: String = "",
    var category: String = "",
    var description: String ="",
    val ownerID: String = "",
    val image: String = "",
): Parcelable
