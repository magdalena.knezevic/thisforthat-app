package com.magdalenaknezevic.loginapp.activites

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.navigation.NavigationView
import com.magdalenaknezevic.loginapp.*
import com.magdalenaknezevic.loginapp.databinding.ActivityMainBinding
import com.magdalenaknezevic.loginapp.databinding.ActivitySignInBinding
import com.magdalenaknezevic.loginapp.firestore.FirestoreClass
import com.magdalenaknezevic.loginapp.fragments.*
import com.magdalenaknezevic.loginapp.models.User
import com.magdalenaknezevic.loginapp.utils.Constants
import com.magdalenaknezevic.loginapp.utils.GlideLoader

//ovdje je prije BaseActivity bio AppCompatActivity, za slucaj da ovako ne radi
open class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var toolbar: Toolbar
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var navigationView: NavigationView
    private lateinit var actionBarDrawerToggle: ActionBarDrawerToggle
    private lateinit var binding: ActivityMainBinding
    private lateinit var textView: TextView
    lateinit var userDetails: User


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        userDetails = User()

        if(intent.hasExtra(Constants.EXTRA_USER_DETAILS)){
            userDetails = intent.getParcelableExtra(Constants.EXTRA_USER_DETAILS)!!
        }

        textView = findViewById(R.id.tv_main)


        toolbar = findViewById(R.id.main_toolbar)
        setSupportActionBar(toolbar)

        drawerLayout = findViewById(R.id.drawer_layout)
        navigationView = findViewById(R.id.nav_view)


       /* var doubleBackToExitPressedOnce = false

        fun doubleBackToExit(){
            if(doubleBackToExitPressedOnce) {
                super.onBackPressed()
                return
            }
            doubleBackToExitPressedOnce = true

            Toast.makeText(
                this,
                resources.getString(R.string.click_again_to_exit),
                Toast.LENGTH_SHORT
            ).show()

            @Suppress("DEPRECATION")
            Handler().postDelayed({doubleBackToExitPressedOnce = false}, 2000)
        }*/

        val backBtn = findViewById<ImageView>(R.id.backButton)

        backBtn.setOnClickListener{
            //super.onBackPressed()
            //supportFragmentManager.popBackStackImmediate()
            replaceFragments(HomeFragment(), "Home")
        }

        val actionBarDrawerToggle = ActionBarDrawerToggle(
            this,
            drawerLayout,
            toolbar,
            R.string.openNavDrawer,
            R.string.closeNavDrawer
        )
        drawerLayout.addDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.run {
            drawerLayout.addDrawerListener(this)
            syncState()
        }


        val frag = supportFragmentManager.beginTransaction()
        frag.replace(R.id.fragment_container, HomeFragment()).commit()

        navigationView.setNavigationItemSelectedListener{

                it.isChecked = true

                when(it.itemId){
                    R.id.profile -> replaceFragments(ProfileFragment(), it.title.toString())
                     R.id.products -> replaceFragments(MyProductsFragment(), it.title.toString())
                     R.id.orders -> replaceFragments(OrdersFragment(), it.title.toString())
                     R.id.info -> replaceFragments(AboutTheAppFragment(), it.title.toString())
                     R.id.logout -> replaceFragments(LogOutFragment(), it.title.toString())
                     R.id.home -> replaceFragments(HomeFragment(), it.title.toString())

                }
                true
            }

        // searchButton = findViewById<ImageView>(R.id.ivSearch)

        //searchButton.setOnClickListener{
        //    replaceFragments(ListAllProductsFragment(), "All Products")
       // }


        }
    private fun replaceFragments(fragment: Fragment, title: String){
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragment_container, fragment)
        fragmentTransaction.commit()
        drawerLayout.closeDrawers()
        setTitle(title)
    }
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        //doubleBackToExit()
    }

    private fun getUserDetails(){
        //showProgressDialog(resources.getString(R.string.please_wait))
        //FirestoreClass().getUserDetails(this@MainActivity)
    }

    fun userDetailsSuccess(user: User){
        setContentView(R.layout.activity_main)
        //hideProgressDialog()

        /*tv_name.text = "${user.firstName} ${user.lastName}"
        tv_address.text = "${user.address}"*/


    }
}










