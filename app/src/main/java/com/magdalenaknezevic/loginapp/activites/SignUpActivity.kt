package com.magdalenaknezevic.loginapp.activites

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.WindowInsets
import android.view.WindowManager
import android.widget.EditText
import android.widget.RadioButton
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.magdalenaknezevic.loginapp.R
import com.magdalenaknezevic.loginapp.databinding.ActivitySignUpBinding
import com.magdalenaknezevic.loginapp.firestore.FirestoreClass
import com.magdalenaknezevic.loginapp.fragments.ProductDetailsFragment
import com.magdalenaknezevic.loginapp.fragments.ProfileFragment
import com.magdalenaknezevic.loginapp.models.User
import com.magdalenaknezevic.loginapp.utils.Constants

class SignUpActivity : BaseActivity() {

    private lateinit var binding: ActivitySignUpBinding
    private lateinit var firebaseAuth : FirebaseAuth
    private val db = Firebase.firestore


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySignUpBinding.inflate(layoutInflater) //inicijaliziranje varijable binding, tipa ActivitySignUp
        setContentView(binding.root)

        @Suppress("DEPRECATION")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
        }

        firebaseAuth = FirebaseAuth.getInstance()

        binding.textView.setOnClickListener {
            val intent = Intent(this, SignInActivity::class.java)
            startActivity(intent)
        }

        binding.button.setOnClickListener{
            val firstName = binding.nameEt.text.toString()
            val lastName = binding.surnameEt.text.toString()
            val email = binding.emailEt.text.toString()
            val pass = binding.passwordEt.text.toString()
            val confirmPass = binding.confirmPasswordEt.text.toString()
            val mobile = binding.mobileEt.text.toString()
            val address = binding.addressEt.text.toString()
            val radioGroup = binding.rgGender
            val gender = findViewById<RadioButton>(radioGroup.checkedRadioButtonId).text.toString()

            if(firstName.isNotEmpty() &&
                lastName.isNotEmpty() &&
                email.isNotEmpty() &&
                pass.isNotEmpty() &&
                confirmPass.isNotEmpty() &&
                mobile.isNotEmpty() &&
                address.isNotEmpty() &&
                gender.isNotEmpty()){
                if(pass == confirmPass){
                    //showProgressDialog("Please Wait...")
                    firebaseAuth.createUserWithEmailAndPassword(email, pass).addOnCompleteListener{
                        if(it.isSuccessful){
                            val firebaseUser: FirebaseUser = it.result!!.user!!

                            val id = firebaseUser.uid

                            val user = User(
                                id,
                                firstName,
                                lastName,
                                pass,
                                email,
                                mobile,
                                gender,
                                address
                            )
                            db.collection(Constants.USERS)
                                .document(id)
                                .set(user)
                                .addOnSuccessListener {
                                    userRegistrationSuccess()
                                    val intent = Intent(this, SignInActivity::class.java)
                                    startActivity(intent)
                                }
                                .addOnFailureListener{ e ->
                                    Log.e(
                                        javaClass.simpleName,
                                        "Error while registering the user",
                                        e
                                    )
                                }

                        } else {
                            //hideProgressDialog()
                            Toast.makeText(this, it.exception.toString(), Toast.LENGTH_SHORT).show()
                        }
                    }
                } else {
                    //hideProgressDialog()
                    Toast.makeText(this, "Password is not matching", Toast.LENGTH_SHORT).show()
                }
            } else {
                //hideProgressDialog()
                Toast.makeText(this, "Empty Fields are not allowed !!",Toast.LENGTH_SHORT).show()
            }


            val profileFragment = ProfileFragment()
            val bundle = Bundle()
            bundle.putString("username", firstName)
            profileFragment.arguments = bundle
        }


    }

    fun userRegistrationSuccess(){

        //hideProgressDialog()

        Toast.makeText(this@SignUpActivity,
                        resources.getString(R.string.register_successfully),
                        Toast.LENGTH_SHORT
        ).show()
    }
}