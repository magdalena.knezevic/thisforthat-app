package com.magdalenaknezevic.loginapp.activites

import android.Manifest
import android.app.Activity
import android.app.Instrumentation
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.Glide.init
import com.magdalenaknezevic.loginapp.R
import com.magdalenaknezevic.loginapp.firestore.FirestoreClass
import com.magdalenaknezevic.loginapp.models.User
import com.magdalenaknezevic.loginapp.utils.Constants
import com.magdalenaknezevic.loginapp.utils.GlideLoader
import java.io.IOException

inline fun <reified T : View> View.find(id: Int): T = findViewById(id) as T
inline fun <reified T : View> Activity.find(id: Int): T = findViewById(id) as T
inline fun <reified T : View> Fragment.find(id: Int): T = view?.findViewById(id) as T

class UserProfileActivity : BaseActivity(), View.OnClickListener {

    lateinit var userDetails: User
    private var mSelectedImageFileUri: Uri? = null
    private var mUserProfileImageURL: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)

        userDetails = User()

        /*if(userDetails.profileCompleted == 0){
            findViewById<TextView>(R.id.tv_title).text = resources.getString(R.string.title_complete_profile)
            findViewById<EditText>(R.id.et_first_name).isEnabled = false
            findViewById<EditText>(R.id.et_first_name).setText(userDetails.firstName)

            findViewById<EditText>(R.id.et_last_name).isEnabled = false
            findViewById<EditText>(R.id.et_last_name).setText(userDetails.lastName)

            findViewById<EditText>(R.id.et_email).isEnabled = false
            findViewById<EditText>(R.id.et_email).setText(userDetails.email)
        } else {
            findViewById<TextView>(R.id.tv_title).text = resources.getString(R.string.title_edit_profile)
            GlideLoader(this@UserProfileActivity).loadUserPicture(userDetails.image, findViewById(R.id.iv_user_photo))
            findViewById<EditText>(R.id.et_first_name).setText(userDetails.firstName)
            findViewById<EditText>(R.id.et_last_name).setText(userDetails.lastName)
        }*/


            findViewById<EditText>(R.id.et_first_name).isEnabled = false
            findViewById<EditText>(R.id.et_first_name).setText(userDetails.firstName)

            findViewById<EditText>(R.id.et_last_name).isEnabled = false
            findViewById<EditText>(R.id.et_last_name).setText(userDetails.lastName)

            findViewById<EditText>(R.id.et_email).isEnabled = false
            findViewById<EditText>(R.id.et_email).setText(userDetails.email)

            findViewById<ImageView>(R.id.iv_user_photo).setOnClickListener(this@UserProfileActivity)
            findViewById<Button>(R.id.btn_submit).setOnClickListener(this@UserProfileActivity)

    }

    override fun onClick(p0: View?) {
        if(p0 != null){
            when(p0.id){

                R.id.iv_user_photo -> {
                    if(ContextCompat.checkSelfPermission(
                            this, Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                    == PackageManager.PERMISSION_GRANTED){
                        //showErrorSnackBar("You already have the storage permission", false)
                        Constants.showImageChooser(this@UserProfileActivity)
                    } else {
                        ActivityCompat.requestPermissions(
                            this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), Constants.READ_STORAGE_PERMISSION_CODE
                        )
                    }
                }

                R.id.btn_submit -> {
                    if(validateUserProfileDetails()){

                        //showProgressDialog(resources.getString(R.string.please_wait))

                        if(mSelectedImageFileUri !=  null) {
                           // FirestoreClass().uploadImageToCloudStorage(this, mSelectedImageFileUri)
                        } else {
                            updateUserProfileDetails()
                        }
                    }
                }
            }
        }
    }

    private fun updateUserProfileDetails(){
        val userHashMap = HashMap<String, Any>()

        val mobileNumber =
            findViewById<EditText>(R.id.et_phone_number).text.toString()
                .trim { it <= ' ' }
        val gender = if (findViewById<RadioButton>(R.id.rb_female).isChecked) {
            Constants.FEMALE
        } else {
            Constants.MALE
        }

        if(mUserProfileImageURL.isNotEmpty()){
            userHashMap[Constants.IMAGE] = mUserProfileImageURL
        }

        if (mobileNumber.isNotEmpty()) {
            userHashMap[Constants.MOBILE] = mobileNumber.toLong()
        }
        //key: gender value:male
        //gender: male
        userHashMap[Constants.GENDER] = gender

        userHashMap[Constants.COMPLETE_PROFILE]=1

        //showProgressDialog(resources.getString(R.string.please_wait))

       // FirestoreClass().updateUserProfileData(this, userHashMap)

    }

    fun userProfileUpdateSuccess(){
        //hideProgressDialog()
        Toast.makeText(
            this@UserProfileActivity,
            resources.getString(R.string.msg_profile_update_success),
            Toast.LENGTH_SHORT
        ).show()

        //startActivity(Intent(this@UserProfileActivity, MainActivity::class.java))
       // finish()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == Constants.READ_STORAGE_PERMISSION_CODE){
            if(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                //showErrorSnackBar("The storage permission is granted", false)
                Constants.showImageChooser(this@UserProfileActivity)
            } else {
                Toast.makeText(
                    this,
                    resources.getString(R.string.read_storage_permission_denied),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }


    //ovo ne radi, popraviti !!!!!!!!!
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?){
        super.onActivityResult(requestCode, resultCode, data)
            if(resultCode == Activity.RESULT_OK){
                if(resultCode == Constants.PICK_IMAGE_REQUEST_CODE){
                    if(data != null){
                        try {
                            mSelectedImageFileUri = data.data!!

                            //findViewById<ImageView>(R.id.iv_user_photo).setImageURI(selectedImageFileUri)
                            GlideLoader(this).loadUserPicture(mSelectedImageFileUri!!, findViewById(R.id.iv_user_photo))
                        } catch(e: IOException) {
                            e.printStackTrace()
                            Toast.makeText(
                                this@UserProfileActivity,
                                resources.getString(R.string.image_selection_failed),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
            } else if(resultCode == Activity.RESULT_CANCELED){
                Log.e("Request Cancelled", "Image selection cancelled")
            }
    }

    private fun validateUserProfileDetails(): Boolean{
        return when {
            TextUtils.isEmpty(findViewById<EditText>(R.id.et_phone_number).text.toString().trim { it <= ' ' }) -> {
               // showErrorSnackBar(resources.getString(R.string.err_msg_enter_mobile_number), true)
                false
            }
            else -> {
                true
            }

        }
    }

    fun imageUploadSuccess(imageURL: String){
        //hideProgressDialog()
        mUserProfileImageURL=imageURL
        updateUserProfileDetails()
    }
}