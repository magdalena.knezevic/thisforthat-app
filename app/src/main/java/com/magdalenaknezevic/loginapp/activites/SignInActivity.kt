package com.magdalenaknezevic.loginapp.activites

//import com.magdalenaknezevic.loginapp.firestore.FirestoreClass
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.WindowInsets
import android.view.WindowManager
import android.widget.Toast
import androidx.fragment.app.FragmentTransaction
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.magdalenaknezevic.loginapp.R
import com.magdalenaknezevic.loginapp.databinding.ActivitySignInBinding
import com.magdalenaknezevic.loginapp.fragments.HomeFragment
import com.magdalenaknezevic.loginapp.fragments.ProfileFragment
import com.magdalenaknezevic.loginapp.models.User

class SignInActivity : BaseActivity() {

    private lateinit var binding: ActivitySignInBinding
    private lateinit var firebaseAuth : FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySignInBinding.inflate(layoutInflater)
        setContentView(binding.root)

        @Suppress("DEPRECATION")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
        }

        firebaseAuth = FirebaseAuth.getInstance()
        val homeFragment = HomeFragment()

        binding.textView.setOnClickListener{
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }

        binding.button.setOnClickListener {
            //showProgressDialog("Please Wait...")

            val email = binding.emailEt.text.toString()
            val pass = binding.passwordEt.text.toString()

            if(email.isNotEmpty() && pass.isNotEmpty()){
                    firebaseAuth.signInWithEmailAndPassword(email, pass).addOnCompleteListener{

                        //hideProgressDialog()

                        if(it.isSuccessful){
                            val firebaseUser: FirebaseUser = it.result!!.user!!

                            val id = firebaseUser.uid
                            //FirestoreClass().getUserDetails(this@SignInActivity)
                            //val intent = Intent(this, BaseActivity::class.java)
                            //val id = firebaseUser.uid
                            //intent.putExtra("user_id", id)
                            //startActivity(intent)
                            //val mFragmentManager = supportFragmentManager
                            //val mFragmentTransaction = mFragmentManager.beginTransaction()
                            //val mFragment = binding.ProfileFragment()

                            //val bundle = Bundle()
                            //bundle.putString("user_id", id)
                            //homeFragment.arguments = bundle
                            //supportFragmentManager.beginTransaction()
                            //    .add(android.R.id.content, MessagesFragment()).commit()
                            val intent = Intent(this, MainActivity::class.java)
                            intent.putExtra("user_id", id)
                            startActivity(intent)
                            //val fragmentTransaction: FragmentTransaction = supportFragmentManager.beginTransaction()
                            //fragmentTransaction.replace(
                            //    android.R.id.content,
                            //    homeFragment
                            //)
                            //fragmentTransaction.commit()
                            //val mBundle = Bundle()
                            //mBundle.putString("mText", mEditText.text.toString())
                            //mFragment.arguments = mBundle
                            //mFragmentTransaction.add(R.id.ProfileLayout, mFragment).commit()
                        } else{
                            Toast.makeText(this, it.exception.toString(), Toast.LENGTH_SHORT).show()
                        }
                    }
            } else {
                Toast.makeText(this, "Empty Fields are not allowed !!", Toast.LENGTH_SHORT).show()
            }



        }

    }

    //override fun onStart() {
     //   super.onStart()

       // if(firebaseAuth.currentUser != null){
       //     val intent = Intent(this, MainActivity::class.java)
       //     startActivity(intent)
       // }
   // }

    fun userLoggedSuccess(user: User){
        //hideProgressDialog()
        val mFragmentManager = supportFragmentManager
        val mFragmentTransaction = mFragmentManager.beginTransaction()
        val mFragment = ProfileFragment()

        //if(user.profileCompleted == 0){
           // val intent = Intent(this@SignInActivity, UserProfileActivity::class.java)
           // intent.putExtra(Constants.EXTRA_USER_DETAILS, user) //parcelable, pravljenje objekta od usera
           // startActivity(intent)
            val mBundle = Bundle()
            //mBundle.putString("mText", mEditText.text.toString())
            mFragment.arguments = mBundle
            mFragmentTransaction.add(R.id.layout, mFragment).commit()
        //} else {
            //startActivity(Intent(this@SignInActivity, MainActivity::class.java))
        //}
        finish()
    }
}