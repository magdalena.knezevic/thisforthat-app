package com.magdalenaknezevic.loginapp.utils

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.provider.MediaStore
import android.webkit.MimeTypeMap

object Constants {
    const val READ_STORAGE_PERMISSION_CODE = 2
    const val USERS: String = "users"
    const val THISFORTHAT_PREFERENCES: String = "ThisForThatPrefs"
    const val LOGGED_IN_USERNAME: String = "logged_in_username"
    const val LOGGED_IN_EMAIL: String = "logged_in_email"
    const val EXTRA_USER_DETAILS: String = "extra_user_details"
    const val PICK_IMAGE_REQUEST_CODE = 1  //moze pisati bilo koji broj

    const val FEMALE: String = "female"
    const val MALE: String = "male"

    const val MOBILE: String = "mobile"
    const val GENDER: String = "gender"
    const val IMAGE: String = "image"
    const val COMPLETE_PROFILE: String = "profileCompleted"

    const val USER_PROFILE_IMAGE: String = "User_Profile_Image"

    fun showImageChooser(activity: Activity) {
        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )

        activity.startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST_CODE)
    }

    fun getFileExtension(activity: Activity, uri: Uri?): String? {
        //5:22:17 iz videa


        return MimeTypeMap.getSingleton()
            .getExtensionFromMimeType(activity.contentResolver.getType(uri!!))
            //.getExtensionFromMimeType(uri?.let { activity?.contentResolver?.getType(it) }).toString()
    }

    const val PRODUCTS: String = "products"
    const val PRODUCT_NAME: String = "product_name"
    const val PRODUCT_CATEGORY: String = "product_category"
    const val PRODUCT_DESCRIPTION: String = "product_description"
    const val COMPLETE_PRODUCT: String = "addNewProductCompleted"
}