package com.magdalenaknezevic.loginapp.utils

import com.magdalenaknezevic.loginapp.models.Product
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface FakerEndPoints {
    @GET("products")
    fun getProducts(@Query("_quantity") quantity: Int): Call<ArrayList<Product>>
}